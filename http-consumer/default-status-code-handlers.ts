import { StatusCodes } from "http-status-codes";

import { ClientHttpException } from "../http-adapter/HttpException";
import {
  handleClientExceptionByStatus,
  StatusCodeHandlersMap,
} from "../http-adapter/HttpStatusCodeHandler";
import { errorMessageText } from "./error-messages";
import { showErrorMessage } from "./show-error-message";

const defaultStatusCodeHandler: StatusCodeHandlersMap<ClientHttpException> = {
  [StatusCodes.UNAUTHORIZED]: () =>
    showErrorMessage(errorMessageText[StatusCodes.UNAUTHORIZED]),
  [StatusCodes.FORBIDDEN]: () =>
    showErrorMessage(errorMessageText[StatusCodes.FORBIDDEN]),
  [StatusCodes.NOT_FOUND]: () =>
    showErrorMessage(errorMessageText[StatusCodes.NOT_FOUND]),
  [StatusCodes.REQUEST_TOO_LONG]: () =>
    showErrorMessage(errorMessageText[StatusCodes.REQUEST_TOO_LONG]),
  [StatusCodes.TOO_MANY_REQUESTS]: () =>
    showErrorMessage(errorMessageText[StatusCodes.TOO_MANY_REQUESTS]),
  default: () => showErrorMessage(errorMessageText.CLIENT_ERROR_DEFAULT),
};

/**
 * Create a ClientHttpExceptionHandler function that call given callbacks according to the status code of the exception.
 * @example
 *  .handleClientException( handleClientExceptionByStatus({
 *    StatusCodes.NOT_FOUND: (clientError) => alert(ReasonPhrases.NOT_FOUND),
 *    StatusCodes.UNAUTHORIZED: (clientError) => alert(ReasonPhrases.UNAUTHORIZED),
 *    default: (clientError) => alert(ReasonPhrases.UNPREDICTED),
 *  })
 * @param statusCodeHandlers - A map of status code handlers
 */
function handleClientExceptionByStatusWithDefaults<
  ClientError extends ClientHttpException
>(statusCodeHandlers: StatusCodeHandlersMap<ClientError>) {
  return handleClientExceptionByStatus({
    ...defaultStatusCodeHandler,
    ...statusCodeHandlers,
  });
}

export { handleClientExceptionByStatusWithDefaults as handleClientExceptionByStatus };
