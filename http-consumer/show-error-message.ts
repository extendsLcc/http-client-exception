/**
 * Make easy to change method of showing error message in entire application.
 * @param message text to be show
 */
export function showErrorMessage(message: string) {
  alert(message); // this could be changed to anything and will be reflected anywhere that showErrorMessage has been used
}
