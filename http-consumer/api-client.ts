import axios from "axios";
import { AxiosHttpClient } from "../http-adapter/AxiosHttpClient";

export const http = new AxiosHttpClient(axios.create({
    baseURL: "http://localhost:8080"
}));
