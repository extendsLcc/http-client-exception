# Overview

Este diretório contem exemplos de declarar configurações padrões para tratativa de exceções consumindo as classes do diretório http-adapter.

## api-client.ts

Cria uma uma instancia de um http client utilizando o axios.

## default-exception-handler-factory.ts

Pré-configura métodos de tratativa de erros padrões com mensagens padrões.

```typescript
import { fromHttpException } from "./default-exception-handler-factory.ts";

http
  .post()
  .then(() => {
    // ...
  })
  .catch((error) => {
    fromHttpException(error).executeHandler();
    // mesmo não declarando nenhum método handler, um dos handlers padrões declarados no arquivo default-exception-handler-factory.ts irá ser executado
  });
```

## default-status-code-handlers.ts

Pré-configura métodos de erros padrões para cada status code especifico.

```typescript
import { handleClientExceptionByStatus } from "./default-status-code-handlers.ts";

http
  .post()
  .then(() => {
    // ...
  })
  .catch((error) => {
    fromHttpException(error)
      .setClientExceptionHandler(handleClientExceptionByStatus({}))
      .executeHandler();
    // mesmo não declarando nenhuma chave de status code ou chave default (similar ao switch case), um dos handlers padrões declarados no arquivo default-status-code-handlers.ts irá ser executado caso o status code da ClientHttpException seja igual ao status code da chave.
  });
```

#### error-messages.ts

Apenas declaração de mensagens de erro padrão do projeto, são apenas um exemplo, isso pode ser lidado de forma totalmente diferente de acordo com o contexto de cada projeto


#### show-error-message.ts

Apenas uma abstração de como mostrar uma mensagem de erro, apenas exemplo, isso pode ser lidado de forma totalmente diferente de acordo com o contexto de cada projeto.