import { StatusCodes } from "http-status-codes";

export const errorMessageText = {
  OFFLINE_ERROR: "offline error message example",
  SERVER_ERROR: "error caused by the server message example",
  UNPREDICTED_ERROR: "unpredicted error message example",
  [StatusCodes.UNAUTHORIZED]: "UNAUTHORIZED message example",
  [StatusCodes.FORBIDDEN]: "FORBIDDEN message example",
  [StatusCodes.NOT_FOUND]: "NOT_FOUND message example",
  [StatusCodes.REQUEST_TOO_LONG]: "REQUEST_TOO_LONG message example",
  [StatusCodes.TOO_MANY_REQUESTS]: "TOO_MANY_REQUESTS message example",
  CLIENT_ERROR_DEFAULT: "default 400 error message example",
};
