import { HttpException } from "../http-adapter/HttpException";
import { fromHttpException } from "../http-adapter/HttpExceptionHandler";
import { handleClientExceptionByStatus } from "./default-status-code-handlers";
import { errorMessageText } from "./error-messages";
import { showErrorMessage } from "./show-error-message";

const fromHttpExceptionWithDefaultHandlers = (exception: unknown) =>
  fromHttpException(exception as HttpException)
    .setClientExceptionHandler(handleClientExceptionByStatus({}))
    .setServerExceptionHandler(() =>
      showErrorMessage(errorMessageText.SERVER_ERROR)
    )
    .setOfflineExceptionHandler(() =>
      showErrorMessage(errorMessageText.OFFLINE_ERROR)
    )
    .setUnpredictedExceptionHandler(() =>
      showErrorMessage(errorMessageText.UNPREDICTED_ERROR)
    );

export { fromHttpExceptionWithDefaultHandlers as fromHttpException };
