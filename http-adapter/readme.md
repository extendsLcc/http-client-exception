# Overview

Este diretório contem um contrato de implementação de um client http (como o axios ou fetch) e Exceptions customizadas para auxiliar a identificação de possiveis erros durante a execução de uma requisição.

## HttpClient.ts

Contrato que define o comportamento de um client http.

## Exception.ts

Classe de Erro javascript para ser extendida por todas as Exceptions customizadas. Pode ser usada por toda a aplicação e não apenas para o propósito deste pacote.

## HttpException.ts

Classes de Exceptions customizadas para tratamento de erros durante a execução de uma requisição http. São usadas para facilitar a identificação do erro ocorrido através do "instanceof" e do Http Status Code. É possivel tipar o corpo do erro para todas as Exceptions.

| Exception               | Uso                                                                      |
| ----------------------- | ------------------------------------------------------------------------ |
| ClientHttpException     | Usada em casos de status code >= 400 && < 500 && != 422                  |
| ServerHttpException     | Usada em casos de status code >= 500                                     |
| ValidationHttpException | Usada especificamente para errors de validação do laravel com status 422 |
| OfflineException        | Usada caso a requisição tenha falhado por conexão indisponivel           |
| UnpredictedException    | Usada para qualquer Erro não previsto pelos anteriores                   |

##### ValidationHttpException

Exceção customizada para tratamento de erros de validação de dados (status 422). Facilita o acesso dos dados de erro de validação do laravel com FormRequest, com a possibilidade de tipar os dados de erro a partir dos dados enviados por uma request.

exemplo:

```typescript

type ProductCreateRequest = {
    name: string;
    description: string;
    price: number;
    categoryId: number;
}

http.post<ProductCreateRequest>()
    .then(() => {
        // ...
    })
    .catch(error => {
        if( error.status === 422 ) {
            const validationException = new ValidationHttpException<ValidationError<ProductCreateRequest>>(error);
            console.log(validationException.details);
            /*
             {
                name: [
                    'The name field is required.'
                ],
                description: [
                    'The description field is required.'
                ],
                price: [
                    'The price field is required.'
                ],
                categoryId: [
                    'The category id field is required.'
                ]
             }
            */
        }
    }
```

nota: utilizando o HttpExceptionHandler o processo fica mais simples.

## HttpExceptionHandler.ts

Contrato e classe para auxiliar na declaração de métodos para tratar erros de exceções especificas dado determinada HttpException com tipagem desconhecida.

```typescript
type RequestInput = {...}
type RequestInputError = ValidationError<RequestInput>;

http.post<RequestInput>()
    .then(() => {
        // ...
    })
    .catch(error /* error is a subclass of HttpException, but its type is still unknown */ => {
      fromHttpException(error)
        .setServerExceptionHandler((serverError) => showErrorMessage('server error', serverError))
        .setClientExceptionHandler((clientError) => showErrorMessage('client error', clientError))
        .setValidationExceptionHandler<RequestInputError>((validationError) => showErrorMessage('validation error', validationError))
        .executeHandler(); // handling is lazy, so it will be executed only when executeHandler is called
    }
```

Mesmo com o erro possuindo o type unknown, o handler identifica a instancia da exception e executa o seu respectivo handler.

## HttpStatusCodeHandler.ts

Função helper para auxiliar a criação de handlers para erros de status code especificos. Recebe um objeto com as chaves de status code e o valor um método handler que será executado caso o erro tenha o mesmo status code que a chave.

```typescript
const clientExceptionMethodHandler = handleClientExceptionByStatus({
  [StatusCodes.NOT_FOUND]: (clientError) => showErrorMessage('not found', clientError),
  [StatusCodes.UNAUTHORIZED]: (clientError) => showErrorMessage('unauthorized', clientError),
  default: (clientError) => showErrorMessage('error didn\'t fall in any other status code', clientError)),
});

fromHttpException(error)
    .setClientExceptionHandler(clientExceptionMethodHandler)
    .executeHandler();

```

## AxiosHttpClient.ts

Implementação do client http utilizando o axios.
É necessario passar uma instancia configurada do axios para o construtor.

Envia requisições e extrai os dados do corpo da resposta (não da acesso direto ao objeto response - caso isso seja necessario, ajustes serão deveram serem feitos), em caso de falha na requisição, encapsula o erro com a devida classe de HttpExcetion, que deverá ser tratada externamente, utilizando o HttpExceptionHandler por exemplo ou outra alternativa.

```typescript
const axiosHttpClient = new AxiosHttpClient(
  axios.create({
    baseURL: "http://localhost:8000/api/",
  })
);

type ProductInput = {
    name: string;
    description: string;
    price: number;
    categoryId: number;
};
type ValidationError<ProductInput>;

axiosHttpClient
  .post<ProductInput>("products")
  .then(() => {
    // ...
  })
  .catch((error) => {
    fromHttpException(error)
      .setServerExceptionHandler((serverError) =>
        showErrorMessage("server error", serverError)
      )
      .setClientExceptionHandler((clientError) =>
        showErrorMessage("client error", clientError)
      )
      .setValidationExceptionHandler<ValidationError>((validationError) =>
        showErrorMessage("validation error", validationError)
      )
      .executeHandler();
  });
```

# Exemplo

No geral serão usados apenas esses 4 items: instancia do AxiosHttpClient, fromHttpException, handleClientExceptionByStatus e o type ValidationError.

```typescript

const http = new AxiosHttpClient(
  axios.create({
    baseURL: "http://localhost:8000/api/",
  })
);

type ProductInput = {
    name: string;
    description: string;
    price: number;
    categoryId: number;
};
type CreateProductValidationError = ValidationError<ProductInput>;

axiosHttpClient
  .post<ProductInput>("products")
  .then(() => {
    // ...
  })
  .catch((error) => {
    fromHttpException(error)
      .setServerExceptionHandler((serverError) =>
        showErrorMessage("server error", serverError)
      )
      .setClientExceptionHandler(
        handleClientExceptionByStatus({
          [StatusCodes.NOT_FOUND]: (clientError) =>
            showErrorMessage("not found", clientError),
          [StatusCodes.UNAUTHORIZED]: (clientError) =>
            showErrorMessage("unauthorized", clientError),
          default: (clientError) =>
            showErrorMessage("error didn't fall in any other status code", clientError),
        })
      )
      .setValidationExceptionHandler<CreateProductValidationError>((validationError) =>
        showErrorMessage("validation error", validationError)
      )
      .executeHandler();
  });
```
